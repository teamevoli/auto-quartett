﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoQuartett.Classes;
using AutoQuartett.Structs;
using System.Collections;
namespace AutoQuartett
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int Menu = 0;
            do
            {
                string[] data = CSVFunc.getData("auto_data.csv");
                Card[] cards = Cards.convertArrayToSturct(data);
                string[] SArray = new string[4] { "1.Spielen", "2.Neue Karte erstellen", "3.Info", "4.Beenden" };//Menu Array
                Menu = cursor_menu(SArray, "Bitte wählen Sie mit den Pfeiltasten einen Punkt aus\nund bestätigen Sie mit ENTER.\n");
                //Console.WriteLine("Sie haben folgenden Menüpunkt gewählt: " + Menu); //Der Menü-Methode ein String-Array mit allen Menüpunkten geben. Man erhält ein int mit der Zahl des Menüpunktes zurück.
                        
                switch (Menu)
                {
                    case 1: //Spielen / Match
                         
                            string[] car_names = new string[cards.Length];
                            int i = 0;
                            foreach (Card card in cards)
                            {
                                car_names[i] = card.Name;
                                i++;
                            }

                            int carone = cursor_menu(car_names, "Bitte wählen Sie ein Auto aus:\nAuto 1: " + "\nAuto 2: "+ "\n");
                            int cartwo = cursor_menu(car_names, "Bitte wählen Sie das zweite Auto aus:\nAuto 1: " + cards[carone-1].Name + "\nAuto 2: "+ "\n");
                        
                            string[] Kategorien = new string[8] { "Geschwindigkeit", "Leistung", "Verbrauch", "Zylinder","Hubraum","Beschleunigung","Zuladung","Ladevolumen" };//Menu Array
                            int category = cursor_menu(Kategorien, "Bitte wählen Sie eine Kategorie aus:\nAuto 1: " + cards[carone-1].Name + "\nAuto 2: " + cards[cartwo-1].Name + "\n");
                            Cards.matchCars(carone-1, cartwo-1, category, cards);
                        Console.Write("\nWeiter mit BELIEBIGE TASTE...");
                        Console.ReadKey();
                        break;
                    case 2://Karte erstellen
                        
                        Cards.createCard();
                        Console.Write("\nWeiter mit BELIEBIGE TASTE...");
                        Console.ReadKey();
                        break;
                    case 3: //Info
                        Console.Clear();
                        Console.WriteLine("\t/////Info/////");
                        Console.WriteLine("Program:\t\tAuto-Quartett");
                        Console.WriteLine("Version:\t\t1.4");
                        Console.WriteLine("Erstelldatum:\t\tSommer 2015");
                        Console.WriteLine("Gruppe:\t\t\tTeam Evoli");
                        Console.WriteLine("Gruppenmitglieder:\tAnnika Windel");
                        Console.WriteLine("\t\t\tLilli Rohe");
                        Console.WriteLine("\t\t\tLukas Kämmerling");
                        Console.WriteLine("\t\t\tVolker Zimmermann");
                        Console.Write("\nWeiter mit BELIEBIGE TASTE...");
                        Console.ReadKey();
                        break;
                    case 4: //Das Programm beenden
                        Environment.Exit(0);
                        break;

                }
            } while (Menu != 4);

           
            
        }

        static int cursor_menu(string[] array, string text)
        {
            int cursor = 0; //Cursor Position
            bool bestaetigen = true;

            Console.Clear();
            Console.WriteLine(text);
            for (int i = 0; i < array.Length; i++)
            {
                if (i == cursor)
                {
                    Console.WriteLine(">" + array[i]);
                }
                else
                {
                    Console.WriteLine(" " + array[i]);
                }
            }

            do
            {
                ConsoleKeyInfo key_druck = Console.ReadKey(); //Key abfragen

                if (cursor > 0 && key_druck.Key == ConsoleKey.UpArrow)
                {
                    cursor--;
                    Console.Clear();
                    Console.WriteLine(text);
                    for (int i = 0; i < array.Length; i++)
                    {
                        if (i == cursor)
                        {
                            Console.WriteLine(">" + array[i]);
                        }
                        else
                        {
                            Console.WriteLine(" " + array[i]);
                        }
                    }
                }
                else if (cursor < array.Length - 1 && key_druck.Key == ConsoleKey.DownArrow)
                {
                    cursor++;
                    Console.Clear();
                    Console.WriteLine(text);
                    for (int i = 0; i < array.Length; i++)
                    {
                        if (i == cursor)
                        {
                            Console.WriteLine(">" + array[i]);
                        }
                        else
                        {
                            Console.WriteLine(" " + array[i]);
                        }
                    }
                }
                else if (key_druck.Key == ConsoleKey.Enter)
                {
                    bestaetigen = false; //wenn ein Menüpunkt mit ENTER gewählt wurde, dann ende der Schleife
                }
            } while (bestaetigen);

            return (cursor + 1); //+1 weil der curser bei 0 und nicht bei 1 beginnt
        }

    }
}


