﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoQuartett.Structs
{
    public struct Card
    {
        public string Name;
        public uint Geschwindigkeit;
        public uint Leistung;
        public double Verbrauch;
        public uint Zylinder;
        public double Hubraum;
        public double Beschleunigung;
        public uint Zuladung;
        public uint Ladevolumen;
    }
}
