﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace AutoQuartett.Classes
{
    using Structs;
    class Cards
    {
        public static Card[] convertArrayToSturct(string[] zeilen)
        {
            Structs.Card[] data = new Structs.Card[zeilen.Length-1];
            int i = 0;
            string[] subzeilen;
            foreach (string zeile in zeilen)
            {
                if (i > 0)
                {
                    subzeilen = zeile.Split(';');
                    Structs.Card tmp = new Structs.Card();
                    tmp.Name = subzeilen[0];
                    tmp.Geschwindigkeit = Convert.ToUInt32(subzeilen[1]);
                    tmp.Leistung = Convert.ToUInt32(subzeilen[2]);
                    tmp.Verbrauch = Convert.ToDouble(subzeilen[3]);
                    tmp.Zylinder = Convert.ToUInt32(subzeilen[4]);
                    tmp.Hubraum = Convert.ToDouble(subzeilen[5]);
                    tmp.Beschleunigung = Convert.ToDouble(subzeilen[6]);
                    tmp.Zuladung = Convert.ToUInt32(subzeilen[7]);
                    tmp.Ladevolumen = Convert.ToUInt32(subzeilen[8]);
                    data[i-1] = tmp;
                }
                i++;

            }
            return data;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cards"></param>
        /// <returns></returns>
        public static string[] convertCardsToCSVArray(Card[] cards)
        {
            string[] csv = new string[cards.Length + 1];
            csv[0] = "Bezeichnung;Geschwindigkeit;Leistung;Verbauch;Zylinder;Hubraum;Beschleunigung;Zuladung;Ladevolumen;";
            int i = 1;
            foreach(Card card in cards){
                csv[i] = card.Name+";"+card.Geschwindigkeit+";"+card.Leistung+";"+card.Verbrauch+";"+card.Zylinder+";"+card.Hubraum+";"+card.Beschleunigung+";"+card.Zuladung+";"+card.Ladevolumen+";";
                i++;
            }
            return csv;
        }
        // By Lilli
        //Die Methode vergleicht die zwei gewählten Karten in der gewählten Kategorie, gibt dem Benutzer aus, welches Auto gewonnen hat und gibt das Auto als integer wieder zurück.
        public static int matchCars(int autoChosenOne, int autoChosenTwo, int category, Card[] cars)
        {
            int i = 2;
            switch (category)
            {
                    //Da i=2 gesetzt wurde, wird i nur geändert, falls Auto 2 in der gewählten Kategorie NICHT gewonnen hat.
                case 1: if (cars[autoChosenOne].Geschwindigkeit > cars[autoChosenTwo].Geschwindigkeit) i = 1;
                    break;
                case 2: if (cars[autoChosenOne].Leistung > cars[autoChosenTwo].Leistung) i = 1;
                    break;
                case 3: if (cars[autoChosenOne].Verbrauch < cars[autoChosenTwo].Verbrauch) i = 1;
                    break;
                case 4: 
                    if (cars[autoChosenOne].Zylinder > cars[autoChosenTwo].Zylinder) i = 1;
                    break;
                case 5: if (cars[autoChosenOne].Hubraum > cars[autoChosenTwo].Hubraum) i = 1;
                    break;
                case 6: if (cars[autoChosenOne].Beschleunigung < cars[autoChosenTwo].Beschleunigung) i = 1;
                    break;
                case 7: if (cars[autoChosenOne].Zuladung > cars[autoChosenTwo].Zuladung) i = 1;
                    break;
                case 8: if (cars[autoChosenOne].Ladevolumen > cars[autoChosenTwo].Ladevolumen) i = 1;
                    break;
                default: i = 3; break;
            }
            if (i != 3)
            {
                string name = "Fehler";
                if (i == 1)
                {
                    name = cars[autoChosenOne].Name;
                }
                else
                {
                    name = cars[autoChosenTwo].Name;
                }
                Console.WriteLine("\nDer {0} hat gewonnen!!!", name);
            }
            else if (i == 3) 
                Console.WriteLine("Beide Autos haben die gleichen Werte in dieser Kategorie");
            else 
                Console.WriteLine("Ihr Vergleich wurde leider nicht verstanden");
            return i;
        }

        public static Card createCard()
        {
            Card newCard;

            Console.Write("Autoname: ");
            newCard.Name = Console.ReadLine();
            
            Console.Write("Geschwindigkeit: ");
            newCard.Geschwindigkeit = Convert.ToUInt32(Console.ReadLine());
            if (newCard.Geschwindigkeit > 250)
            {
                newCard.Geschwindigkeit = 250;
            }
            Console.Write("Leistung: ");
            newCard.Leistung = Convert.ToUInt32(Console.ReadLine());
            
            Console.Write("Verbrauch: ");
            newCard.Verbrauch = Convert.ToDouble(Console.ReadLine());
            
            Console.Write("Zylinder: ");
            newCard.Zylinder = Convert.ToUInt32(Console.ReadLine());
            
            Console.Write("Hubraum: ");
            newCard.Hubraum = Convert.ToDouble(Console.ReadLine());
            
            Console.Write("Beschleunigung: ");
            newCard.Beschleunigung = Convert.ToDouble(Console.ReadLine());
            
            Console.Write("Zuladung: ");
            newCard.Zuladung = Convert.ToUInt32(Console.ReadLine());
            
            Console.Write("Ladevolumen: ");
            newCard.Ladevolumen = Convert.ToUInt32(Console.ReadLine());
            Card[] tmp = new Card[1];
            tmp[0] = newCard;
            var merged = Cards.convertArrayToSturct(CSVFunc.getData("auto_data.csv")).Concat(tmp);
            var merged_as_obj = merged.ToArray();
            Card[] karten = merged.ToArray() ;
            CSVFunc.writeData("auto_data.csv", Cards.convertCardsToCSVArray(karten));
            return newCard;
        }
    }
}
