﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoQuartett.Structs;
namespace AutoQuartett.Classes
{
    class CSVFunc
    {
    /// <summary>
    /// Holt alle Daten aus einer CSV und gibt diese als Array (Cards Array zurück)
    /// </summary>
    /// <param name="filename">Pfad zur Datei</param>
    /// <returns>CardsArray mit allen Karten aus der CSV</returns>
        public static string[] getData(string filename)
        {
            if (File.Exists(filename))
            {
                string[] zeilen = File.ReadAllLines(filename);         
                return zeilen;

            }
            else
            {

                throw new SystemException("Datei " + filename + " nicht gefunden");
            }
            
        }
        /// <summary>
        /// Speichert die Daten als Datei
        /// </summary>
        /// <param name="filename">Pfad zur Datei</param>
        /// <param name="data">die kompletten Daten</param>
        /// <returns>bool</returns>
        public static bool writeData(string filename, string[] data){
           
            File.WriteAllLines(filename, data);
            return File.Exists(filename);
        }


    }
}
